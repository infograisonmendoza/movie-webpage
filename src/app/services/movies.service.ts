import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class MoviesService {
  API = "http://www.omdbapi.com/?i=tt3896198&apikey=e375fb13&s=";

  constructor(private http: HttpClient) { }

  getMovies(search:string) {
    return this.http.get<MoviesService>(this.API+search);
  }

  searchMovie(search: string) {
    return this.http.get<MoviesService>(this.API + search);
  }

  getPage (search:string,page:any) {
    return this.http.get<MoviesService>(`${this.API}${search}&page= ${page}`)
  }
}
