import { Component } from "@angular/core";
import { MoviesService } from "./services/movies.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent {
  public total: string = "";
  public movies: Array<any> = [];
  private result: Array<any> = [];
  private page: Array<any> = [];

  searchData: string = "going";
  value: string = "";
  pageLocation: any = 1;

  constructor(private movieService: MoviesService) {
    this.movieService.getMovies(this.searchData).subscribe((res: any) => {
      this.movies = res.Search;
      this.total = res.totalResults;
      this.pageLocation = 1;
    });
  }

  inputSearch() {
    if (this.searchData.trim() && this.searchData.trim().length >= 3) {
      this.value = this.searchData;

      this.movieService.searchMovie(this.value).subscribe((res: any) => {
        this.result = res.Search;
        this.total = res.totalResults;
        this.movies = this.result;
        this.pageLocation = 1;
      });
    }
  }

  prevPage() {
    if (this.movies.length && this.pageLocation >= 2) {
      this.pageLocation = this.pageLocation - 1;
      this.movieService
        .getPage(this.searchData, this.pageLocation)
        .subscribe((res: any) => {
          this.page = res.Search;
          this.total = res.totalResults;
          this.movies = this.page;
        });
    }
  }

  nextPage() {
    if (this.movies.length && this.pageLocation <= this.pageLocation) {
      this.pageLocation = this.pageLocation + 1;
      this.movieService
        .getPage(this.searchData, this.pageLocation)
        .subscribe((res: any) => {
          this.page = res.Search;
          this.movies = this.page;
        });
    }
  }
}
